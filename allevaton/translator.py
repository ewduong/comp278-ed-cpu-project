#!/usr/bin/python3
#
#	Translates Brainfuck code into primitive CPU-readable opcodes
#

import argparse, io, os

parser = argparse.ArgumentParser( description='Translate brainfuck file', prog='Brainfuck Translator' )

parser.add_argument( 'file', type=str, help='The input files' )
parser.add_argument( 'out', type=str, help='The input files',  )

args = parser.parse_args()

f = open( args.file, 'r' ) 
out = open( args.out, 'w+' )

# Define this here just so we know we got here.
def translate( c ):
	if   c is '<':
		return '0'
	elif c is '>':
		return '1'
	elif c is '+':
		return '2'
	elif c is '-':
		return '3'
	elif c is '.':
		return '4'
	elif c is ',':
		return '5'
	elif c is '[':
		return '6'
	elif c is ']':
		return '7'
	else:
		return ''
	
try:
	translated = ''
	characters = []
	
	# Write out Logisim's header
	out.write( 'v2.0 raw\n' )
	
	# Read the input file and translate the characters into a buffer
	for c in f.read():
		translated += translate( c )
	
	# Get past the v2.0 raw section (\n counts!)
	out.seek( 0 )
	out.seek( 9 )
	
	previous = 0
	while True:
		chars = ''
		
		try:
			# Try to grab 8 characters from the buffer
			for i in range( 8 ):
				chars += translated[i + previous]
			previous += 8
		except IndexError as e:
			# It's okay if it fails.
			pass
		
		# If there ARE 8 characters, meaning it didn't fail...
		if len( chars ) == 8:
			# Put them into the output buffer
			characters.append( chars )
		else:
			characters.append( chars ) 
			break
	
	# And write them by line
	for line in characters:
		# Then by character
		for c in line:
			out.write( c + ' ' )
		out.write( '\n' )
		
	print( 'File translated successfully to ' + out.name )
	
finally:
	# Safely close them
	f.close()
	out.close()
