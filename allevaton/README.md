# Brainfuck
### What is Brainfuck?
Brainfuck is a programming language that can only process shifting left and
right registers, incrementing and decrementing the value of the current
register, keyboard input and display output, and loops. Using these limited
instructions, we can actually perform quite the amount of functionality.

### How can these instructions be read by the CPU?
They can't. The instructions need to be, in a sense, compiled into "machine
code," which basically 8 instructions (0-7). The characters of Brainfuck are
compiled into code the CPU can interpret. For example:
```brainfuck
++
[
 > +
 < -
]>
> ++++++++++
```

This code above would be translated into:
```compiled_brainfuck
v2.0 raw
2 2 6 1 2 0 3 7 
1 1 2 2 2 2 2 2 
2 2 2 2 
```

### If it needs to be compiled, how can I write code in Brainfuck?
It's easy: you can use the given compiler. The compiler is a command-line only
utility and can be used quite easily. On UNIX or Mac OS, you can execute it
using:
``` UNIX_shell
$ ./compile.py <input.bf> <output>
```

On Windows, make sure the python3.3 directory is in your system PATH, then you
can use:
``` Win32_dos
> python compile.py <input.bf> <output>
```

Don't worry about whitespace, the compiler should take it out for you and
compile your code just fine. You can then load this right into the CPU, run it,
and see if it works!

### And keyboard input works, too?
Certainly. The way it currently works, is it sees what is currently in the 
keyboard buffer, grabs the first character out, and stores it in the current
register. It can do this over and over, so you can make a program to display
"Hello world," without even incrementing or decrementing.
```brainfuck
,.>,.>,.>,.>,.>,.>,.>,.>,.>,.>,.>
```
As long as the keyboard buffer says **exactly** "Hello world", then "Hello
world" will be printed into the output TTY (teletype).